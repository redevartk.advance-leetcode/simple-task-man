# [Advance Leetcode] Simple Task Manager

Simple task management, this is front-end only

## Properties

* Programming Lang: Typescript
* Framework: Angular 17.0.2 + Ionic 7.2.0

## Installation

1. Clone repo
2. run "npm install"
3. run "ionic serve"

## Developer Note

1. The front-end is not done yet, but can be tested on "delete selected", and "mark complete at"
